project "spirv"
	language "C++"
	kind "StaticLib"

	systemversion "latest"

	files
	{
		"GlslangToSpv.cpp",
		"InReadableOrder.cpp",
		"Logger.cpp",
		"SpvBuilder.cpp",
		"SpvPostProcess.cpp",
		"doc.cpp",
		"SpvTools.cpp",
		"disassemble.cpp",
		"SPVRemapper.cpp",
		"doc.cpp"
	}

	filter "configurations:debug"
		targetsuffix "d"
		defines
		{
			"DEBUG"
		}
		symbols "On"

	filter "configurations:release"
		defines
		{
			"NDEBUG"
		}
		optimize "On"

	filter "platforms:x86"
		architecture "x32"

	filter "platforms:x64"
		architecture "x64"
