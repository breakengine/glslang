workspace "glslang"
	configurations {"debug", "release"}
	platforms {"x86", "x64"}
	location "build"
	targetdir "bin/%{cfg.platform}/%{cfg.buildcfg}/"
	defaultplatform "x64"

	include "OGLCompilersDLL"
	include "glslang"
	include "spirv"
	include "hlsl"
