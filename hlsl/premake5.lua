project "hlsl"
	language "C++"
	kind "StaticLib"

	systemversion "latest"

	files
	{
		"hlslAttributes.cpp",
		"hlslParseHelper.cpp",
		"hlslScanContext.cpp",
		"hlslOpMap.cpp",
		"hlslTokenStream.cpp",
		"hlslGrammar.cpp",
		"hlslParseables.cpp",
		"pch.cpp"
	}

	filter "configurations:debug"
		targetsuffix "d"
		defines
		{
			"DEBUG",
			"ENABLE_HLSL=1"
		}
		symbols "On"

	filter "configurations:release"
		defines
		{
			"NDEBUG",
			"ENABLE_HLSL=1"
		}
		optimize "On"

	filter "platforms:x86"
		architecture "x32"

	filter "platforms:x64"
		architecture "x64"
