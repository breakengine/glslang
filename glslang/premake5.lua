if os.ishost("windows") then
	include "OSDependent/Windows"
elseif os.ishost("linux") then
	include "OSDependent/Unix"
end

project "glslang"
	language "C++"
	kind "StaticLib"

	systemversion "latest"

	files
	{
		"MachineIndependent/glslang_tab.cpp",
		"MachineIndependent/attribute.cpp",
		"MachineIndependent/Constant.cpp",
		"MachineIndependent/iomapper.cpp",
		"MachineIndependent/InfoSink.cpp",
		"MachineIndependent/Initialize.cpp",
		"MachineIndependent/IntermTraverse.cpp",
		"MachineIndependent/Intermediate.cpp",
		"MachineIndependent/ParseContextBase.cpp",
		"MachineIndependent/ParseHelper.cpp",
		"MachineIndependent/PoolAlloc.cpp",
		"MachineIndependent/RemoveTree.cpp",
		"MachineIndependent/Scan.cpp",
		"MachineIndependent/ShaderLang.cpp",
		"MachineIndependent/SymbolTable.cpp",
		"MachineIndependent/Versions.cpp",
		"MachineIndependent/intermOut.cpp",
		"MachineIndependent/limits.cpp",
		"MachineIndependent/linkValidate.cpp",
		"MachineIndependent/parseConst.cpp",
		"MachineIndependent/reflection.cpp",
		"MachineIndependent/preprocessor/Pp.cpp",
		"MachineIndependent/preprocessor/PpAtom.cpp",
		"MachineIndependent/preprocessor/PpContext.cpp",
		"MachineIndependent/preprocessor/PpScanner.cpp",
		"MachineIndependent/preprocessor/PpTokens.cpp",
		"MachineIndependent/propagateNoContraction.cpp",
		"GenericCodeGen/CodeGen.cpp",
		"GenericCodeGen/Link.cpp"
	}

	filter "configurations:debug"
		targetsuffix "d"
		defines
		{
			"DEBUG"
		}
		symbols "On"

	filter "configurations:release"
		defines
		{
			"NDEBUG"
		}
		optimize "On"

	filter "platforms:x86"
		architecture "x32"

	filter "platforms:x64"
		architecture "x64"
